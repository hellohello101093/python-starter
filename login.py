user_names = ['mike684',
            'jason748',
            'helena808',
            'jack894',
            'jeff065',
            'david649',
            'tommy694',
            'jacky502',
            'era143',
            'winston133',
            'becky766',
            'brandy916',
            'victoria685',
            'lenon996']
passwords = ['afsdf23',
            'sdfdadf3454',
            '3445adf',
            '4543r43r345',
            '34rreeaw4a',
            '45gertg34t',
            'hhthsertrh',
            'errytw555',
            'gty54y45gt',
            'aw543t54y',
            'rt5gtsgrg54',
            'qt443gerg',
            'q45yyhrtye',
            '4t5tw45']
role_id = [ 111,
            111,
            112,
            111,
            113,
            111,
            112,
            112,
            111,
            111,
            111,
            111,
            111,
            112]
role_name = {111: 'normal', 112: 'admin', 113: 'superadmin'}

roles = []
for ids in role_id :
    roles.append(role_name[ids])


username = input("Username: ")
user_name_length = len(username)
valid_username = username[-3:].isdigit()


login_ok = False
valid_password = False
user_name_index = -1


while login_ok == False:
    while valid_username == False:
        print ('Invalid username. Please enter your username again')
        username = input("Username: ")
        user_name_length = len(username)
        valid_username = username[-3:].isdigit()
    try:
        user_name_index = user_names.index(username)
    except:
        user_name_index = -1
    password = input("Password: ")

    if password != passwords[user_name_index]:
        print ('Wrong username or password.')
        valid_username = False
    else:
        login_ok = True
print ('Welcome {}, {} privilege granted'.format ( username.capitalize(),roles[user_name_index])  )      
if roles[user_name_index] == 'superadmin':
    info = input ("View info of users: ")
    info_exist= -1
    try:
        info_exist = user_names.index(info)
    except:
        info_exist = -1
    while info_exist== -1:
        print ('No user with this username')
        info=input ("View info of users: ")
        info_exist= -1
        try:
            info_exist = user_names.index(info)
        except:
            info_exist = -1
    print('User: {} \nPassword: {} \nRole: {}'.format(user_names[info_exist],passwords[info_exist],roles[info_exist]))
 