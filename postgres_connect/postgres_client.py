import psycopg2
from postgres_config import config

class PostgresClient:
    def __init__(self, config_file):
        self.config_file = config_file

    def connect(self):
        params = config(self.config_file)
        conn = psycopg2.connect(**params)
        self.cursor = conn.cursor()

    def exec_query(self, query):
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result
